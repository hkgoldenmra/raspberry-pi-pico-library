import digitalio

class HD61202:

	def __init__(self, rsPin, rwPin, enPin, dPins, cPins, rtPin):
		self.__data = [[False] * 192] * 64
		self.__rs = digitalio.DigitalInOut(rsPin)
		self.__rs.direction = digitalio.Direction.OUTPUT
		self.__rw = digitalio.DigitalInOut(rwPin)
		self.__rw.direction = digitalio.Direction.OUTPUT
		self.__en = digitalio.DigitalInOut(enPin)
		self.__en.direction = digitalio.Direction.OUTPUT
		self.__ds = []
		for dPin in dPins:
			d = digitalio.DigitalInOut(dPin)
			self.__ds.append(d)
		self.__cs = []
		for cPin in cPins:
			c = digitalio.DigitalInOut(cPin)
			c.direction = digitalio.Direction.OUTPUT
			self.__cs.append(c)
		self.__rt = digitalio.DigitalInOut(rtPin)
		self.__rt.direction = digitalio.Direction.OUTPUT
		self.__rt.value = True

	def __set(self, cs, rs, data):
		for c in range(len(cs)):
			self.__cs[c].value = cs[c]
		self.__rs.value = (rs & 0x01) > 0
		self.__rw.value = False
		self.__en.value = True
		for i in range(len(self.__ds)):
			self.__ds[i].direction = digitalio.Direction.OUTPUT
			self.__ds[i].value = (data & (1 << i)) > 0
		self.__en.value = False

	# Sets the display on or off
	def setDisplay(self, cs, data):
		self.__set(cs, False, 0x3E | ((data & 0x01) > 0))

	# Sets the cursor to specific column
	def setColumn(self, cs, data):
		self.__set(cs, False, 0x40 | (data & 0x3F))

	# Sets the cursor to specific row
	def setRow(self, cs, data):
		self.__set(cs, False, 0xB8 | (data & 0x07))

	# Sets the start line
	def setLine(self, cs, data):
		self.__set(cs, False, 0xC0 | (data & 0x3F))

	# Writes 8-bit data into DDRAM of cursor position
	def setData(self, cs, data):
		self.__set(cs, True, data & 0xFF)

	# Sets the boolean data to specific (x,y) position (top left corner is (0,0))
	def setXY(self, x, y, data):
		cs = [True, True]
		if x < 64:
			cs = [False, False]
		elif x < 128:
			cs = [True, False]
		elif x < 192:
			cs = [False, True]
		r = y // 8
		self.__data[y][x] = (data & 0x01) > 0
		self.setRow(cs, r)
		self.setColumn(cs, x % 64)
		data = 0
		for i in range(8):
			if self.__data[r * 8 + i][x]:
				data |= (1 << i)
		self.setData(cs, data)

	def __get(self, cs, rs):
		data = 0
		for c in range(len(cs)):
			self.__cs[c].value = cs[c]
		self.__rs.value = (rs & 0x01) > 0
		self.__rw.value = True
		self.__en.value = True
		for i in range(len(self.__ds)):
			self.__ds[i].direction = digitalio.Direction.INPUT
			data |= self.__ds[i].value << i
		self.__en.value = False
		return data

	# Gets the status of HD61202
	def getStatus(self, cs):
		return self.__get(cs, False)

	# Gets the data of the cursor position
	def getData(self, cs):
		return self.__get(cs, True)