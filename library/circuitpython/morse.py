import digitalio
import time

class Morse:

	DELAY_SHORT = 0.06
	DELAY_LONG = DELAY_SHORT * 2

	ALPHANUMERIC_PATTERNS = {}
	ALPHANUMERIC_PATTERNS["0"] = [True, True, True, True, True]
	ALPHANUMERIC_PATTERNS["1"] = [False, True, True, True, True]
	ALPHANUMERIC_PATTERNS["2"] = [False, False, True, True, True]
	ALPHANUMERIC_PATTERNS["3"] = [False, False, False, True, True]
	ALPHANUMERIC_PATTERNS["4"] = [False, False, False, False, True]
	ALPHANUMERIC_PATTERNS["5"] = [False, False, False, False, False]
	ALPHANUMERIC_PATTERNS["6"] = [True, False, False, False, False]
	ALPHANUMERIC_PATTERNS["7"] = [True, True, False, False, False]
	ALPHANUMERIC_PATTERNS["8"] = [True, True, True, False, False]
	ALPHANUMERIC_PATTERNS["9"] = [True, True, True, True, False]
	ALPHANUMERIC_PATTERNS["A"] = [False, True]
	ALPHANUMERIC_PATTERNS["B"] = [True, False, False, False]
	ALPHANUMERIC_PATTERNS["C"] = [True, False, True, False]
	ALPHANUMERIC_PATTERNS["D"] = [True, False, False]
	ALPHANUMERIC_PATTERNS["E"] = [False]
	ALPHANUMERIC_PATTERNS["F"] = [False, False, True, False]
	ALPHANUMERIC_PATTERNS["G"] = [True, True, False]
	ALPHANUMERIC_PATTERNS["H"] = [False, False, False, False]
	ALPHANUMERIC_PATTERNS["I"] = [False, False]
	ALPHANUMERIC_PATTERNS["J"] = [False, True, True, True]
	ALPHANUMERIC_PATTERNS["K"] = [True, False, True]
	ALPHANUMERIC_PATTERNS["L"] = [False, True, False, False]
	ALPHANUMERIC_PATTERNS["M"] = [True, True]
	ALPHANUMERIC_PATTERNS["N"] = [True, False]
	ALPHANUMERIC_PATTERNS["O"] = [True, True, True]
	ALPHANUMERIC_PATTERNS["P"] = [False, True, True, False]
	ALPHANUMERIC_PATTERNS["Q"] = [True, True, False, True]
	ALPHANUMERIC_PATTERNS["R"] = [False, True, False]
	ALPHANUMERIC_PATTERNS["S"] = [False, False, False]
	ALPHANUMERIC_PATTERNS["T"] = [True]
	ALPHANUMERIC_PATTERNS["U"] = [False, False, True]
	ALPHANUMERIC_PATTERNS["V"] = [False, False, False, True]
	ALPHANUMERIC_PATTERNS["W"] = [False, True, True]
	ALPHANUMERIC_PATTERNS["X"] = [True, False, False, True]
	ALPHANUMERIC_PATTERNS["Y"] = [True, False, True, True]
	ALPHANUMERIC_PATTERNS["Z"] = [True, True, False, False]

	def __init__(self, pin):
		self.__pin = digitalio.DigitalInOut(pin)
		self.__pin.direction = digitalio.Direction.OUTPUT
		self.__pin.value = False

	def send(self, text):
		text = text.upper()
		for char in text:
			if char in Morse.ALPHANUMERIC_PATTERNS:
				for b in Morse.ALPHANUMERIC_PATTERNS[char]:
					self.__pin.value = True
					time.sleep(Morse.DELAY_SHORT)
					if b:
						time.sleep(Morse.DELAY_LONG)
					self.__pin.value = False
					time.sleep(Morse.DELAY_SHORT)
				time.sleep(Morse.DELAY_LONG)