import digitalio
import math
import time

class ST7789:

	def __init__(self, resPin, dcPin, csPin, sclPin, sdaPin):
		self.__res = digitalio.DigitalInOut(resPin)
		self.__res.direction = digitalio.Direction.OUTPUT
		self.__res.value = True
		self.__dc = digitalio.DigitalInOut(dcPin)
		self.__dc.direction = digitalio.Direction.OUTPUT
		self.__cs = digitalio.DigitalInOut(csPin)
		self.__cs.direction = digitalio.Direction.OUTPUT
		self.__cs.value = True
		self.__scl = digitalio.DigitalInOut(sclPin)
		self.__scl.direction = digitalio.Direction.OUTPUT
		self.__sda = digitalio.DigitalInOut(sdaPin)
		self.__sda.direction = digitalio.Direction.OUTPUT

	def hardwareReset(self):
		self.__res.value = False
		time.sleep(1)
		self.__res.value = True

	def __transfer(self, data):
		for i in reversed(range(8)):
			self.__scl.value = False
			self.__sda.value = (data & (1 << i)) > 0
			self.__scl.value = True

	def __send(self, command, parameters):
		self.__cs.value = False
		self.__dc.value = False
		self.__transfer(command)
		if len(parameters) > 0:
			self.__dc.value = True
			for parameter in parameters:
				self.__transfer(parameter)
		self.__cs.value = True

	def softwareReset(self):
		self.__send(0x01, [])

	def setSleepMode(self, data):
		self.__send(0x10 | data, [])

	def setPartialMode(self, data):
		self.__send(0x12 | data, [])

	def setInverseMode(self, data):
		self.__send(0x20 | data, [])

	def setDisplayMode(self, data):
		self.__send(0x28 | data, [])

	def setTearingMode(self, data):
		self.__send(0x34 | data, [])

	def setIdleMode(self, data):
		self.__send(0x38 | data, [])

	def setColorMode(self, data):
		self.__send(0x3A, [data])

	def fillRect(self, x1, y1, x2, y2, c):
		w = abs(x1 - x2) + 1
		h = abs(y1 - y2) + 1
		self.__cs.value = False
		self.__dc.value = False
		self.__transfer(0x2A)
		self.__dc.value = True
		self.__transfer((x1 >> 8) & 0xFF)
		self.__transfer(x1 & 0xFF)
		self.__transfer((x2 >> 8) & 0xFF)
		self.__transfer(x2 & 0xFF)
		self.__dc.value = False
		self.__transfer(0x2B)
		self.__dc.value = True
		self.__transfer((y1 >> 8) & 0xFF)
		self.__transfer(y1 & 0xFF)
		self.__transfer((y2 >> 8) & 0xFF)
		self.__transfer(y2 & 0xFF)
		self.__dc.value = False
		self.__transfer(0x2C)
		self.__dc.value = True
		for j in range(h):
			for i in range(w):
				self.__transfer((c >> 16) & 0xFF);
				self.__transfer((c >> 8) & 0xFF);
				self.__transfer(c & 0xFF);
		self.__cs.value = True

	def setPixel(self, x, y, c):
		self.fillRect(x, y, x, y, c)

	def drawRect(self, x1, y1, x2, y2, c):
		self.fillRect(x1, y1, x2 - 1, y1, c)
		self.fillRect(x2, y1, x2, y2 - 1, c)
		self.fillRect(x1, y1 + 1, x1, y2, c)
		self.fillRect(x1 + 1, y2, x2, y2, c)

	def drawLine(self, x1, y1, x2, y2, c):
		if x1 == x2 or y1 == y2:
			if x2 < x1:
				x1, x2 = x2, x1
			if y2 < y1:
				y1, y2 = y2, y1
			self.fillRect(x1, y1, x2, y2, c)
		else:
			h = y2 - y1
			w = x2 - x1
			a = h / w
			b = y1 - a * x1
			w = abs(w) + 1
			h = abs(h) + 1
			if h < w:
				array = range(x1, x2 + 1, 1)
				if x2 < x1:
					array = range(x1, x2 - 1, -1)
				for x in array:
					y = round(a * x + b)
					self.setPixel(x, y, c)
			else:
				array = range(y1, y2 + 1, 1)
				if y2 < y1:
					array = range(y1, y2 - 1, -1)
				for y in array:
					x = round((y - b) / a)
					self.setPixel(x, y, c)

	def drawLine2(self, x1, y1, x2, y2, c):
		if x1 == x2 or y1 == y2:
			if x2 < x1:
				x1, x2 = x2, x1
			if y2 < y1:
				y1, y2 = y2, y1
			self.fillRect(x1, y1, x2, y2, c)
		else:
			h = y2 - y1
			w = x2 - x1
			a = h / w
			b = y1 - a * x1
			w = abs(w) + 1
			h = abs(h) + 1
			array = range(x1, x2 + 1, 1)
			if x2 < x1:
				array = range(x1, x2 - 1, -1)
			for x in array:
				y = round(a * x + b)
				self.setPixel(x, y, c)

	def drawCircle(self, cx, cy, r, c):
		m = 1
		for i in range(360 * m):
			d = math.radians(i)
			x = cx + round(r * math.sin(d / m))
			y = cy + round(r * math.cos(d / m))
			self.setPixel(x, y, c)