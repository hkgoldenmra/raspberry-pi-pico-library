import digitalio
import time

class HD44780:

	DELAY_CLEAR = 0.002
	DELAY_S = 0.000010

	CURSOR = False
	SCREEN = not CURSOR
	LEFT = False
	RIGHT = not LEFT

	# Constructs a HD44780 object
	def __init__(self, rsPin, rwPin, ePins, dPins):
		self.__rs = digitalio.DigitalInOut(rsPin)
		self.__rs.direction = digitalio.Direction.OUTPUT
		self.__rw = digitalio.DigitalInOut(rwPin)
		self.__rw.direction = digitalio.Direction.OUTPUT
		self.__es = []
		for ePin in ePins:
			e = digitalio.DigitalInOut(ePin)
			e.direction = digitalio.Direction.OUTPUT
			self.__es.append(e)
		self.__ds = []
		for dPin in dPins:
			d = digitalio.DigitalInOut(dPin)
			self.__ds.append(d)

	def __enable(self, signal, index=None):
		time.sleep(HD44780.DELAY_S)
		if index is None:
			for e in self.__es:
				e.value = signal
		else:
			self.__es[index].value = signal

	def __set(self, rs, data, index=None):
		self.__enable(True, index)
		self.__rs.value = (rs & 0x01) > 0
		self.__rw.value = False
		for i in range(len(self.__ds)):
			self.__ds[i].direction = digitalio.Direction.OUTPUT
			self.__ds[i].value = (data & (1 << i)) > 0
		self.__enable(False, index)

	# Replaces all DDRAM with 0x20 character and execute home() method
	def clear(self, index=None):
		self.__set(False, 0x01, index)
		time.sleep(HD44780.DELAY_CLEAR)

	# Sets cursor address to 0
	def home(self, index=None):
		self.__set(False, 0x02, index)

	# Sets cursor move directionand after DDRAM is updated or read
	# setEntry(HD44780.RIGHT, HD44780.CURSOR) as default
	def setEntry(self, direction, mode, index=None):
		data = 0x04
		if (direction & 0x01) > 0:
			data |= 0x02
		if (mode & 0x01) > 0:
			data |= 0x01
		self.__set(False, data, index)

	# Sets screen on/off, cursor on/off, cursor blink on/off
	# setDisplay(True, True, True) as the best settings
	def setDisplay(self, screen, cursor, blink, index=None):
		data = 0x08
		if (screen & 0x01) > 0:
			data |= 0x04
		if (cursor & 0x01) > 0:
			data |= 0x02
		if (blink & 0x01) > 0:
			data |= 0x01
		self.__set(False, data, index)

	# Moves cursor or screen
	def setMovement(self, mode, direction, index=None):
		data = 0x10
		if (mode & 0x01) > 0:
			data |= 0x08
		if (direction & 0x01) > 0:
			data |= 0x04
		self.__set(False, data, index)

	# Sets data bits (4/8), number of display rows (1/2), character dots (8/10)
	# setFunction(True, True, False) as the best 8-bit mode settings
	# setFunction(False, True, False) as the best 4-bit mode settings
	def setFunction(self, bits, rows, dots, index=None):
		data = 0x20
		if (bits & 0x01) > 0:
			data |= 0x10
		if (rows & 0x01) > 0:
			data |= 0x08
		if (dots & 0x01) > 0:
			data |= 0x04
		self.__set(False, data, index)

	# Creates a custom CGRAM in 0 to 7 indexes
	def setCustom(self, address, data, index=None):
		self.__set(False, 0x40 | ((address & 0x07) << 3), index)
		for d in data:
			self.__set(True, d, index)

	# Sets cursor address to the target position
	# setPosition(0x00) as 1-st row, 1-st column
	# setPosition(0x01) as 1-st row, 2-nd column
	# setPosition(0x40) as 2-nd row, 1-st column
	def setPosition(self, position, index=None):
		self.__set(False, 0x80 | (position & 0x7F), index)

	# Sets cursor address to the target row and column
	def setRowColumn(self, row, column, index=None):
		if row > 0:
			column |= 0x40
		self.setPosition(column, index)

	# Writes 8-bit data into DDRAM of cursor position
	def setData(self, data, index=None):
		self.__set(True, data & 0xFF, index)

	# Writes an ascii character into DDRAM of cursor position
	def setChar(self, char, index=None):
		self.setData(ord(char[0]), index)

	# Writes an ascii text into DDRAM of cursor position
	def setText(self, text, index=None):
		length = len(text)
		for i in range(length):
			self.setChar(text[i], index)

	def __get(self, rs, index):
		data = 0
		self.__enable(True, index)
		self.__rs.value = (rs & 0x01) > 0
		self.__rw.value = True
		self.__enable(False, index)
		for i in range(len(self.__ds)):
			self.__ds[i].direction = digitalio.Direction.INPUT
			data |= self.__ds[i].value << i
		return data

	# Reads the address of cursor position
	def getPosition(self):
		return self.__get(False)

	# Reads 8-bit data from DDRAM of cursor position
	def getData(self):
		return self.__get(True)