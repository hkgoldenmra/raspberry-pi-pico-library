import busio
import time

class DFPlayerMini:

	def DEFAULT_BAUDRATE():
		return 9600

	def DEFAULT_BITS():
		return 8

	def EQ_NORMAL():
		return 0

	def EQ_POP():
		return 1

	def EQ_ROCK():
		return 2

	def EQ_JAZZ():
		return 3

	def EQ_CLASSIC():
		return 4

	def EQ_BASS():
		return 5

	def SOURCE_USB():
		return 1

	def SOURCE_SD():
		return 2

	def __set(self, command, parameterH, parameterL):
		commands = [0x7E, 0xFF, 0x06, command, 0x01, parameterH, parameterL, 0x00, 0x00, 0xEF]
		checksum = -(commands[1] + commands[2] + commands[3] + commands[4] + commands[5] + commands[6]) & 0xFFFF
		commands[7] = (checksum & 0xFF00) >> 8
		commands[8] = checksum & 0x00FF
		self.__uart.write(bytes(commands))
		time.sleep(0.1)

	def __init__(self, txPin, rxPin):
		self.__uart = busio.UART(txPin, rxPin, baudrate=DFPlayerMini.DEFAULT_BAUDRATE(), bits=DFPlayerMini.DEFAULT_BITS(), parity=None)

	def playNext(self):
		return self.__set(0x01, 0x00, 0x00)

	def playPrevious(self):
		return self.__set(0x02, 0x00, 0x00)

	def play(self, value=None):
		if value == None:
			return self.__set(0x0D, 0x00, 0x00)
		else:
			hi = (value & 0xFF00) >> 8
			lo = value & 0x00FF
			return self.__set(0x03, hi, lo)

	def volumeUp(self):
		return self.__set(0x04, 0x00, 0x00)

	def volumeDown(self):
		return self.__set(0x05, 0x00, 0x00)

	def setVolume(self, value):
		return self.__set(0x06, 0x00, value)

	def setEQ(self, value):
		return self.__set(0x07, 0x00, value)

	def loopItem(self, value):
		hi = (value & 0xFF00) >> 8
		lo = value & 0x00FF
		return self.__set(0x08, hi, lo)

	def source(self, value):
		return self.__set(0x09, 0x00, value)

	def shutdown(self):
		return self.__set(0x0A, 0x00, 0x00)

	def pause(self):
		return self.__set(0x0E, 0x00, 0x00)

	def loopAll(self, value):
		return self.__set(0x11, 0x00, value & 0x01)

	def mp3(self, value):
		hi = (value & 0xFF00) >> 8
		lo = value & 0x00FF
		return self.__set(0x12, hi, lo)

	def stop(self):
		return self.__set(0x16, 0x00, 0x00)

	def __get(self, command, parameterH, parameterL):
		value = -1
		bits = 10
		self.__set(command, parameterH, parameterL)
		data = self.__uart.read()
		print(data)
		for i in range(len(data) / bits):
			if data[i * bits + 3] == command:
				value = (data[i * bits + 5] << 16) | data[i * bits + 6]
		return value

	def getVolume(self):
		return self.__get(0x43, 0x00, 0x00)

	def getEQ(self):
		return self.__get(0x44, 0x00, 0x00)

	def getLoop(self):
		return self.__get(0x45, 0x00, 0x00)

	def getVersion(self):
		return self.__get(0x46, 0x00, 0x00)

	def getUSBItemCount(self):
		return self.__get(0x47, 0x00, 0x00)

	def getSDItemCount(self):
		return self.__get(0x48, 0x00, 0x00)

	def getUSBItemPosition(self):
		return self.__get(0x4B, 0x00, 0x00)

	def getSDItemPosition(self):
		return self.__get(0x4C, 0x00, 0x00)